## Enable tracking on FDP portals

Add dependency to your project `package.json` file

`"fdp-util-tracking": "git+https://git.swedbank.net/scm/fdpap/fdp-util-tracking.git#project/integration"`

Import tracking angular module and add it to application's modules

```js
import "fdp-util-tracking";

export default angular
    .module("portal.private", [
        ...
        "util.tracking",
    ]
```

By default tracking is disabled. In order to enable it inject and configure service provider.

```js
ConfigurePortal.$inject = [
    ...
    "util.tracking.TrackerServiceProvider"
];

export function ConfigurePortal(
    ...
    trackerServiceProvider
) {

    trackerServiceProvider.enable({
        portalName: ENV_PORTAL_NAME,
        envProduction: ENV_PRODUCTION
    });

}
```

## Flow tracking

### Create flow tracking

Every single flow has it's own type. Flow types is decribed in `src/tracker/flows/flow-tracking.config.ts` as enum `FLOW`. Before using flows import and inject FlowTrackingManagerService into your application

```js
import { FLOW } from 'fdp-util-tracking';

AppController.$inject = [
    ...
    "util.tracking.FlowTrackingManagerService",
];

function AppController(..., FlowTrackingManagerService) {
    ...

    let flow = FlowTrackingManagerService.get(FLOW.INVESTMENT_FUND_BUY);

}
```

Once flow is created FlowTrackingManagerService stores it's instance. When application tries to access specific flow that was already created it will get that flow's instance. So it's possible to get flow's instance in different controllers of the application.

### Flow methods

#### step(index: number, data: any = {})

| parameter | description                     |
| --------- | ------------------------------- |
| index     | flow step number, starts from 0 |
| data      | key-value object                |

Argument `data` is related to flow's steps configuration which is defined in `src/tracker/flows/flow-tracking.config.ts` as const `FLOWSTEPS`. Every step of configuration has it's own parameters that should be present in `data` argument.

`FLOWSTEPS` explanation:

```ts
[FLOW.ABC]: [
    // first step
    {
        // field "company" is mandatory and it's value has to be string
        company: { type: "string" },
        // field "fundName" is mandatory and it's value has to be string
        fundName: { type: "string" },
    },
    // second step
    {
        // field "recurringType" is optional but if it exists it's value has to be either number or string
        recurringType: { type: ["number", "string"], optional: true },
    },
    ...
 ]

 ...

flow.step(0, { company: "Swedbank", fundName: "Swedbank Robur Technology" });
flow.send();

flow.step(1, { recurringType: 7 });
flow.send();
```

#### stepsAdded():number

Returns number of steps added to a flow.

#### send()

Collects data provided by flow.step methods, merges it and sends to further handling.

#### sendErrors(errors: string[])

Use this function in order to send errors for particular flow step.
Works the same way as `send()` except for it requires an array of errors.

#### reset()

Erases all steps data and resets current step index.

## Click tracking

### Service import and usage example

```js
AppController.$inject = [
    ...
    "util.tracking.ClickService",
];

function AppController(..., ClickService) {
    ...

    ClickService.content.button.send("button_name");

}
```

### Group properties

Click tracking service has predefined group properties which indicate the particular part of the application:

```
leftMenu
header
content
footer
```

### Type properties

Accessing any group property gives an object with predefined type properties stating which particular element of the application was clicked:

```
questionMark
button
link
explanation
tab
openFile
expandable
checkbox
contextualMenu
```

Any group property can be chained with any type property:

```
ClickService.content.button;
ClickService.header.link;
...
```

### Sending click

#### send(name: string, extraParameters: object = {})

| parameter       | description                                         |
| --------------- | --------------------------------------------------- |
| name            | Name of the clicked element                         |
| extraParameters | object with optional properties "flow" and "status" |

-   "flow" - enables to track clicks that happen as a part of the flow
-   "status" - only applicable for click types "expandable" and "checkbox"
    -   "expandable":  
         `status: "expanded" | "collapsed"`
    -   "checkbox":  
        `status: "checked" | "unchecked"`

Examples:

```ts
ClickService.footer.link.send("contacts");
ClickService.content.chekbox.send("accept_rules", { status: "checked" });
ClickService.content.expandable.send(
    "extra_info",
    {
        status: "collapsed",
        flow: {
            type: FLOW.ABC,
            step: 0
        }
    }
);
```

### Setting click tracking on HTML elements

In order to track clicks on HTML elements provide attributes "click-tracking-group", "click-tracking-type" and "click-tracking-name".

| attribute                | description                              |
| ------------------------ | ---------------------------------------- |
| click-tracking-type      | A property name of `CLICK_ELEMENT_TYPE`  |
| click-tracking-group     | A property name of `CLICK_ELEMENT_GROUP` |
| click-tracking-name      | Value to specify click name              |
| click-tracking-flow-type | Optional. Flow type                      |
| click-tracking-flow-step | Optional. Flow step number               |

`CLICK_ELEMENT_GROUP`, `CLICK_ELEMENT_TYPE` can be found `src/tracker/click/click-tracking.config.ts`

Examples:

```html
<div
    click-tracking-type="BUTTON"
    click-tracking-group="HEADER"
    click-tracking-name="contacts"
>Contacts</div>

<div
    click-tracking-type="BUTTON"
    click-tracking-group="HEADER"
    click-tracking-name="contacts"
    click-tracking-flow-type="INVESTMENT_FUND_BUY"
    click-tracking-flow-step="3"
>Contacts</div>
```

## Adform TrackPoint

The tracking is done by adding `swed-adforms-track-point` (notice the typo it is `adforms` not `adform`) component.

**Bindings for swed-adforms-track-point:**

| Binding | Type    | Description                                                                                          |
| ------- | ------- | ---------------------------------------------------------------------------------------------------- |
| swName  | string  | Optional value to append to `$state.current.name` when creating track point identifier               |
| swData  | Promise | Required promise value that must resolve with the data that will be stringified and sent as payload. |

### Replacing swed-adforms-track-point with AdformTrackPointService

**Before**

```html
<swed-adforms-track-point sw-name="optional-name" sw-data="$ctrl.dataPromise"></swed-adforms-track-point>
```

```ts
class SomeComponentController {
    private static $inject = ["$q"];

    constructor(private $q) {}

    public $onInit() {
        this.dataPromise = this.$q.resolve("data");
    }
}
```

**After**

```ts
class SomeComponentController {
    private static $inject = ["util.tracking.AdformTrackPointService"];

    constructor(private AdformTrackPointService) {}

    public $onInit() {
        this.AdformTrackPointService.report("data", "optional-name");
    }
}
```